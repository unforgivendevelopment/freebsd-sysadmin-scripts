# FreeBSD SysAdmin Scripts #
A collection of scripts, created with the intent of simplifying various tasks
that a FreeBSD SysAdmin may encounter in their daily work, as well as a variety
of more obscure tasks, which tend to require research, man pages, Googling, and
therefore: **time**.

## General List of Categories ##
Provided below is a general list of categories that the various scripts may be
organized into. Please note that this is not an all-encompassing list, and some
tasks may fall into multiple categories, or may simply be regarded as "_misc_".

* Packages and Ports
  * Initial Installation
  * Initial Configuration
  * Advanced Configuration and Optimization
  * Log File Management
* Configuration Files
  * Standalone
  * Installer-based
* Misc

## Contact Information ##
These scripts are provided by the team at https://unforgivendevelopment.com

The primary developer on the project is Gerad Munsch, whom may be contacted via
the following methods:
* Twitter: @unforgiven512
* Google+: +GeradMunsch
* Skype: unforgiven512
* GitHub: unforgiven512
* XMPP: gmunsch@unforgivendevelopment.com
* Email: gmunsch@unforgivendevelopment.com

## License Information ##
This project is licensed under a BSD-style license. Please see the **LICENSE**
file for the full details.
