#!/usr/bin/env bash

## define various aspects of the setup procedure ##
pw_new_uid="779"
sys_newsyslog_conf_path="/usr/local/etc/newsyslog.conf.d"
file_monitorix_newsyslog_tmp="/tmp/monitorix-${RANDOM}.tmp"


## FUNCTION: add monitorix user to system db ##
add_monitorix_user() {
	echo -e "\033[1m** Creating new user \033[36;1mmonitorix\033[0m \033[1m with UID \033[36;1m${pw_new_uid}\033[0m \033[1mfor the \"monitorix-httpd\" daemon\033[0m"
	sudo pw user add monitorix -n monitorix -u ${pw_new_uid} -c "Monitorix system monitoring daemon" -d /var/empty -s /usr/sbin/nologin
}

## FUNCTION: install newsyslog.conf.d file ##
install_newsyslog_conf_file() {
	echo -e "\033[1m** Installing \033[36;1mmonitorix.conf\033[0m \033[1mfile to \033[36;1m${sys_newsyslog_conf_path}\033[0m"

	## copy file to /tmp/ ##
	cp ./monitorix-newsyslog.conf "${file_monitorix_newsyslog_tmp}"

	## use sudo to copy to system newsyslog.conf.d path ##
	sudo cp "${file_monitorix_newsyslog_tmp}" "${sys_newsyslog_conf_path}/monitorix.conf"

	## delete tmp file ##
	rm "${file_monitorix_newsyslog_tmp}"
}

## main execution path ##
add_monitorix_user
install_newsyslog_conf_file
